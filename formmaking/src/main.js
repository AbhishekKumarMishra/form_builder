import Vue from 'vue'
import App from './App.vue'
import FormMaking from 'form-making'
import 'form-making/dist/FormMaking.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(FormMaking, {lang: 'en-US'});
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
